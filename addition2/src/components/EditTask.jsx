import React, { useEffect, useState } from 'react'

function EditTask({ task, index, taskList, setTaskList }) {
    const [editModal,setEditModal]=useState(false);
    const [projectName, setProjectName] = useState("");
    const [taskDescription, setTaskDescription] = useState("");
    useEffect(()=>{
        setProjectName(task.projectName);
        setTaskDescription(task.taskDescription);
    },[])
    const handleUpdate = (e) => {
        e.preventDefault();
        let taskIndex=taskList.indexOf(task);
        taskList.splice(taskIndex,1);
        setTaskList(
            [...taskList, { projectName, taskDescription }]
        )
        setEditModal(false);
    }
    const handleInput = e => {
        const { name, value } = e.target;
        if (name === "projectName") setProjectName(value);
        if (name === "taskDescription") setTaskDescription(value);
    }
  return (
    <div>
          <button className='btn btn-secondary' onClick={()=>setEditModal(true)}>Edit</button>
            {
                editModal ?(
                    <>
                      <div className='position-absolute border rounded rounded-3 content'>
                          <div className='bg-white'>
                              <div className='d-flex justify-between py-3 px-4 items-center'>
                                  <h3 className='mx-5'>Edit Task</h3>
                                  <button className='btn btn-danger mx-5' onClick={() => setEditModal(false)}>x</button>
                              </div>
                              <form className='p-6'>
                                  <div className='px-4'>
                                      <label className=' mb-2' htmlFor='project-name font'>Project Name</label>
                                      <br />
                                      <input id="project-name"
                                          value={projectName}
                                          onChange={handleInput}
                                          name='projectName'
                                          type='text'
                                          placeholder='Project name' className='w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-5 leading-tight focus:outline-none focus:bg-white' required />
                                  </div>
                                  <div className='px-4'>
                                      <label className='track-wide uppercase text-gray-700 text-xs font-semibold mb-2' htmlFor='project-name'>
                                          Task Description
                                      </label>
                                      <br />
                                      <textarea id="task-description"
                                          cols="30"
                                          rows="3"
                                          name="taskDescription"
                                          value={taskDescription}
                                          onChange={handleInput}
                                          placeholder='Task description'
                                          className='w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-5 leading-tight focus:outline-none focus:bg-white'
                                      />
                                  </div>
                              </form>
                              <div className='d-flex justify-content-end p-4'>
                                  <button className='btn btn-success ' onClick={handleUpdate}>Update Task</button>
                              </div>
                          </div>


                      </div>
                    </>
                ):(null)
            }
    </div>
  )
}

export default EditTask