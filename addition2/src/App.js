import React from 'react'
import { useState } from 'react'
import AddTask from './components/AddTask'
import ToDo from './components/ToDo';

function App() {
  const [taskList,setTaskList]=useState([]);
  return (
    <div>
      <h1 className='text-2xl font-bold py-4 px-3'>03-The Task Tracker</h1>
      <p className='mx-3'>Hi there!</p>
      <div className='d-flex items-center'>
        <p className='mx-3'>Click</p>
        <AddTask taskList={taskList} setTaskList={setTaskList} />
        <p className='mx-3'>to add a new task</p>
      </div>
      <div>
        <h2 className='mx-3 bg-body-secondary w-50 text-center mb-3'>To Do:</h2>
        {
          taskList.map((task, index) => 
             (
            
                <ToDo key={index} task={task} index={index} taskList={taskList} setTaskList={setTaskList}/>
              
            )
          )
        }
      </div>
      
    </div>
  )
}

export default App